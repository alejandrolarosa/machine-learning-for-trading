from data.get_stock_data.market import Market
from databases.SchemaDb import SchemaDb


class Main():

    def run(self):
        # SchemaDb().create()
        # second step populate all symbol by market.
        # see json files merval.json and cedears.json
        excluded_symbols = list()
        merval = Market(name="merval2", excluded_symbols=excluded_symbols, years=1, days=0)
        merval.populate_symbols()
        # Step 3 - get all data from yahoos
        merval.get_yahoo_data_by_market()


if __name__ == '__main__':
    Main().run()

# first step create schemas in database sqllite
