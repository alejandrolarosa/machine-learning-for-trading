import sqlite3
from sqlite3 import Error
from constants import constants


class SymbolDAO:
    class __DataBases:
        def __init__(self, database):
            self.database = database
            try:
                self.conn = sqlite3.connect(database, check_same_thread=False)
            except Error as e:
                print(e)

        def __str__(self):
            return repr(self) + self.database

    instance = None

    def get_connection(self):
        return self.instance.conn

    def __init__(self, database):
        if not SymbolDAO.instance:
            SymbolDAO.instance = SymbolDAO.__DataBases(database)
        else:
            SymbolDAO.instance.database = database

    # def __getattr__(self, name):
    #     return getattr(self.instance, name)

    def create_symbol_table(self):
        # Get a cursor object
        cursor = self.instance.conn.cursor()
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS symbol(id_yahoo TEXT,id_google TEXT,creationDate DATE, market TEXT,PRIMARY KEY (id_yahoo))

        ''')
        self.instance.conn.commit()

    def insert_symbol(self, symbol, market):
        import datetime
        # Get a cursor object
        cursor = self.instance.conn.cursor()
        try:
            cursor.execute('INSERT INTO symbol(id_yahoo,id_google, creationDate,market) values (?, ? ,?,?)',
                           (symbol, symbol.replace('.BA', ''), datetime.datetime.now().date(), market))
            self.instance.conn.commit()
            return symbol
        except Error as e:
            print "insertQuotation error"
            print(e)

    def select_all_symbols(self, market):
        result = list()
        """
        Query all rows in the tasks table
        :param conn: the Connection object
        :return:
        """
        cur = self.instance.conn.cursor()
        cur.execute("SELECT * FROM symbol WHERE market  = '" + market + "' ORDER BY id_google DESC;")

        rows = cur.fetchall()

        for row in rows:
            result.append(row)
        return result


def main():
    # create a database connection
    db = SymbolDAO(constants.databaseLocation)
    db.create_symbol_table()
    symbol = db.insert_symbol("YPF.BA", "MERVAL")
    db.select_all_symbols('MERVAL')


if __name__ == '__main__':
    main()