import sqlite3
from sqlite3 import Error

from constants.constants import databaseLocation, host, port, username, password, database, databaseType
from databases.SymbolDAO import SymbolDAO
from datetime import datetime
from influxdb import InfluxDBClient
import pytz


class QuotationDAO:
    class __DataBases:
        def __init__(self, database, connection):
            self.database = database
            try:
                if connection:
                    self.conn = connection
                else:
                    self.conn = sqlite3.connect(database)
            except Error as e:
                print(e)

        def __str__(self):
            return repr(self) + self.database

    instance = None

    def __init__(self, database, connection):
        if not QuotationDAO.instance:
            QuotationDAO.instance = QuotationDAO.__DataBases(database, connection)
        else:
            QuotationDAO.instance.database = database

    def __getattr__(self, name):
        return getattr(self.instance, name)

    def create_quotation_table(self):
        # Get a cursor object
        cursor = self.conn.cursor()
        '''
            ['Open' 'High' 'Low' 'Close' 'Adj Close' 'Volume']
        '''
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS quotation(symbol text,
                               creationDate DATE,
                               Open DECIMAL(10,5),
                               High DECIMAL(10,5),
                               Low DECIMAL(10,5),
                               Close DECIMAL(10,5),
                               Adj_Close DECIMAL(10,5),
                               Volume DECIMAL(10,5),
                               PRIMARY KEY (symbol, creationDate),
                               FOREIGN KEY(symbol) REFERENCES symbol(id_google))

        ''')
        self.conn.commit()

    def insert_quotation(self, symbol, market, date_only, open_price, high, low, close, adj_close, volume):

        try:
            if databaseType == 'influx':
                json_body = [
                    {
                        "measurement": "shares_market_price",
                        "tags": {
                            "symbol": symbol,
                            "market": market
                        },
                        "time": date_only.replace(tzinfo=pytz.utc),
                        "fields": {
                            "open": open_price,
                            "high": high,
                            "low": low,
                            "close": close,
                            "adj_close": adj_close,
                            "volume": volume
                        }
                    }
                ]
                client = InfluxDBClient(host=host, port=port, username=username, password=password, database=database)
                client.write_points(json_body)
            else:
                # Get a cursor object
                cursor = self.conn.cursor()
                cursor.execute(
                    'INSERT INTO quotation(symbol, creationDate,Open,High,Low,Close,Adj_Close,Volume) values (?,?,?,'
                    '?,?,?,?,?)',
                    (symbol, date_only, open_price, high, low, close, adj_close, volume))
                self.conn.commit()
        except Error as e:
            print "insertQuotation error"
            print(e)

    def get_symbols_with_quotations(self):
        results = list()
        query = "select id_yahoo from symbol where id in(select symbol from quotation group by symbol);"
        cur = self.conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            results.append(row[0])
        return results

    def select_quote_by_symbol(self, symb):
        result = list()
        """
        Query tasks by priority
        :param conn: the Connection object
        :param priority:
        :return:
        """
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM quotation WHERE symbol=? ORDER BY date(creationDate) ASC", (symb,))

        rows = cur.fetchall()

        for row in rows:
            print row
            quotationDate = datetime.strptime(row[1], "%Y-%m-%d %H:%M:%S")
            print quotationDate
            result.append((quotationDate.date().strftime('%m/%d/%Y'), row[2], row[3], row[4], row[5], row[6], row[7]))
        print result
        return result


def main():
    # create a database connection
    symbolDAO = SymbolDAO(database=databaseLocation)
    db = QuotationDAO(database=databaseLocation, connection=symbolDAO.get_connection())
    symbolDAO.create_symbol_table()
    db.create_quotation_table()
    symbol = symbolDAO.insert_symbol("YPF.BA", "MERVAL")
    import datetime
    db.insert_quotation(symbol, datetime.datetime.now().date(), "120.12", "110.11", "1.1", "1.1", "1.1", "1.1")
    print("1. Query task by priority:")
    db.select_quote_by_symbol("YPF.BA")

    print("2. Query all tasks")
    symbolDAO.select_all_symbols("merval")


if __name__ == '__main__':
    main()
