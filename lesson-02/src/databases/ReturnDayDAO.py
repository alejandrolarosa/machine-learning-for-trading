import sqlite3
from sqlite3 import Error

from constants.constants import databaseLocation


class ReturnDayDAO:
    class __DataBases:
        def __init__(self, database):
            self.database = database
            try:
                self.conn = sqlite3.connect(database)
            except Error as e:
                print(e)

        def __str__(self):
            return repr(self) + self.database

    instance = None

    def __init__(self, database):
        if not ReturnDayDAO.instance:
            ReturnDayDAO.instance = ReturnDayDAO.__DataBases(database)
        else:
            ReturnDayDAO.instance.database = database

    def __getattr__(self, name):
        return getattr(self.instance, name)

    def create_return_day_table(self):
        # Get a cursor object
        cursor = self.conn.cursor()
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS returnDay(symbol Text,
                               creationDate DATE,
                               mean DECIMAL(10,5),
                               std DECIMAL(10,5),
                               kurtosis DECIMAL(10,5),
                               PRIMARY KEY (symbol, creationDate),
                               FOREIGN KEY(symbol) REFERENCES symbol(id))

        ''')
        self.conn.commit()

    def insert_return_day(self, symbol, dateOnly, mean, std, kurtosis):
        # Get a cursor object
        cursor = self.conn.cursor()
        try:
            cursor.execute('INSERT INTO returnDay(symbol, creationDate,mean,std,kurtosis) values (?,?,?,?,?)',
                           (symbol, dateOnly, mean, std, kurtosis))
            self.conn.commit()
        except Error as e:
            print "insertReturnDay error"
            print(e)


def main():
    # create a database connection
    return_day_dao = ReturnDayDAO(database=databaseLocation)
    return_day_dao.create_return_day_table()
    import datetime
    return_day_dao.insert_return_day("YPF.BA", datetime.datetime.now().date(), "0.00164830861986", "0.0115118426122", "1.89244375211")


if __name__ == '__main__':
    main()
