import sqlite3
from sqlite3 import Error

from constants.constants import databaseLocation


class RsiDAO:
    class __DataBases:
        def __init__(self, database):
            self.database = database
            try:
                self.conn = sqlite3.connect(database)
            except Error as e:
                print(e)

        def __str__(self):
            return repr(self) + self.database

    instance = None

    def __init__(self, database):
        if not RsiDAO.instance:
            RsiDAO.instance = RsiDAO.__DataBases(database)
        else:
            RsiDAO.instance.database = database

    def __getattr__(self, name):
        return getattr(self.instance, name)

    def create_rsi_table(self):
        # Get a cursor object
        cursor = self.conn.cursor()
        cursor.execute('''
            CREATE TABLE IF NOT EXISTS rsi(symbol Text,
                               creationDate DATE,
                               simple_value DECIMAL(10,5),
                               exponencial_value DECIMAL(10,5),
                               PRIMARY KEY (symbol, creationDate),
                               FOREIGN KEY(symbol) REFERENCES symbol(id))

        ''')
        self.conn.commit()

    def insert_rsi(self, symbol, date_only, simple, exponential):
        # Get a cursor object
        cursor = self.conn.cursor()
        try:
            cursor.execute('INSERT INTO rsi(symbol, creationDate,simple_value,exponencial_value) values (?,?,?,?)',
                           (symbol, date_only, simple, exponential))
            self.conn.commit()
        except Error as e:
            print "insertRSI error"
            print(e)


def main():
    # create a database connection
    rsi_dao = RsiDAO(database=databaseLocation)
    rsi_dao.create_rsi_table()
    import datetime
    rsi_dao.insert_rsi("YPF.BA", datetime.datetime.now().date(), "10.1", "10.1")


if __name__ == '__main__':
    main()
