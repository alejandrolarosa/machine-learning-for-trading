
from constants import constants

# Create schema in local sqlite DB.
# print(constants.databaseLocation)
class SchemaDb:

    def create(self):
        print constants.databaseLocation
        from databases.SymbolDAO import SymbolDAO
        symbolDAO = SymbolDAO(database=constants.databaseLocation)
        symbolDAO.create_symbol_table()
        from databases.QuotationDAO import QuotationDAO
        quoteDAO = QuotationDAO(constants.databaseLocation ,symbolDAO.get_connection())
        quoteDAO.create_quotation_table()
        from databases.RsiDAO import RsiDAO
        rsi_dao = RsiDAO(database=constants.databaseLocation)
        rsi_dao.create_rsi_table()
