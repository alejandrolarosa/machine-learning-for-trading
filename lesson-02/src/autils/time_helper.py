def get_now():
    import datetime
    return datetime.datetime.now()


def get_date_time(time):
    import datetime
    return datetime.datetime(time.year, time.month, time.day)


def get_delta(now, days):
    from dateutil.relativedelta import relativedelta
    return now - relativedelta(days=days)
