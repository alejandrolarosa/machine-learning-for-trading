import os


# remove file only if exists
def remove_file(name):
    if os.path.exists(name):
        os.remove(name)
    pass


# make dir only if not exists
def make_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)
