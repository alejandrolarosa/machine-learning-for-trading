import os

import pandas as pd

from autils.plot_helper import save_figure
from constants.constants import databaseLocation
from data.constant_stock import ConstantsFile
from databases.QuotationDAO import QuotationDAO
from databases.SymbolDAO import SymbolDAO


def back_dir(market):
    return os.getcwd() + "/" + ConstantsFile().PATH_DATA_STOCK.format(market)


def symbol_to_path(market, symbol):
    """Return CSV file path given ticker symbol."""
    return os.path.join(back_dir(market), "{}.csv".format(str(symbol)))


def plot_histogram(symbols, dates):
    df = get_data(symbols, dates)  # get data for each symbol
    plot_data(df)
    dr = compute_daily_returns(df)
    plot_data(dr, title="Daily returns")
    dr.hist(bins=20, label="SPY")
    for symbol in symbols:
        mean = dr[symbol].mean()
        std = dr[symbol].std()
        kurtosis = dr[symbol].kurtosis()
        print "symbol={}, mean={} , std={} , Kurtosis={}".format(symbol, mean, std, kurtosis)
        # plt.axvline(mean, color='w', linestyle="dashed", linewidth=2)
        # plt.axvline(std, color='r', linestyle="dashed", linewidth=2)
        # plt.axvline(-std, color='r', linestyle="dashed", linewidth=2)


def writeCSV(mylist):
    import csv
    import os
    import tempfile

    open(tempfile.mktemp(), "w")
    filename = "{}/{}.tmp".format(tempfile.gettempdir(), os.getpid())

    with open(filename, "w") as csvfile:
        header = ['Date', 'Open', 'High', 'Low', 'Close', 'Adj Close', 'Volume']
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(header)
        wr = csv.writer(csvfile, quoting=csv.QUOTE_ALL)
        for e in mylist:
            wr.writerow(e)
        return csvfile.name


def get_data(market, symbols, dates, defaultSymbol="YPFD.BA"):
    """Read stock data (adjusted close) for given symbols from CSV files."""
    # populate with dataset of databases
    df_final = pd.DataFrame(index=dates)
    if defaultSymbol not in symbols:  # add SPY for reference, if absent
        symbols.insert(0, defaultSymbol)

    for symbol in symbols:
        db = QuotationDAO(database=databaseLocation, connection=SymbolDAO(database=databaseLocation).get_connection())
        file = writeCSV(db.select_quote_by_symbol(symbol))
        # generate csv file with dataset

        df_temp = pd.read_csv(file, parse_dates=True, index_col="Date",
                              usecols=["Date", "Close"], na_values=["nan"])
        df_temp = df_temp.rename(columns={"Close": symbol})
        df_final = df_final.join(df_temp)
        if symbol == defaultSymbol:  # drop dates SPY did not trade
            df_final = df_final.dropna(subset=[defaultSymbol])

    return df_final


def plot_data(df_data, title="Daily returns Histogram"):
    """Plot stock data with appropriate axis labels."""
    ax = df_data.plot(title=title, fontsize=2)
    ax.set_xlabel("Date")
    ax.set_ylabel("Price")
    ax.set_title(title)
    return ax


# def compute_daily_returns(df):
#     daily_returns = df.copy()
#     daily_returns[1:] = (df[1:] / df.slice_shift(1)) - 1
#     daily_returns.ix[0, :] = 0
#     return daily_returns

def compute_daily_returns(df):
    # Alternative method
    daily_returns = (df / df.shift(1)) - 1
    daily_returns.ix[0, :] = 0

    # Another alternative method
    # daily_returns = df.copy()
    # compute daily returns for row 1 onwards
    # daily_returns[1:] = (daily_returns[1:] / daily_returns[-1:].values) - 1
    # daily_returns.ix[0, :] = 0 # set daily returns for row 0 to 0

    return daily_returns


def plot_rolling_bands(df, base_dir, symbol, suffix):
    rolling_mean = get_rolling_mean(df[symbol], window=14)
    rolling_std = get_rolling_std(df[symbol], window=14)
    upper_band, lower_band = get_bollinger_bands(rolling_mean, rolling_std)

    ax = df[symbol].plot(title='Rolling bands', label=symbol)
    rolling_mean.plot(label='Rolling mean', ax=ax)
    upper_band.plot(label='Upper Band', ax=ax)
    lower_band.plot(label='Lower Band', ax=ax)
    ax.set_xlabel("Date")
    ax.set_ylabel("Price")
    ax.legend(loc='upper left')
    save_figure(plot=ax, base_dir=base_dir, symbol=symbol, suffix=suffix)


def get_rolling_mean(dfCol, window=20):
    return pd.rolling_mean(dfCol, window=window)


def get_bollinger_bands(rm, rstd):
    upper_band = rm + rstd * 1.5
    lower_band = rm - rstd * 1.5
    return upper_band, lower_band


def get_rolling_std(df, window=20):
    return pd.rolling_std(df, window=window)
