

def save_figure(plot,base_dir,symbol,suffix):
    fig = plot.get_figure()
    fig.savefig("{}/{}-{}.png".format(base_dir,symbol,suffix))
    fig.clear()