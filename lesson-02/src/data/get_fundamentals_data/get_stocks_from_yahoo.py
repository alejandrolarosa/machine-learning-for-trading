# coding=utf-8
import re
import urllib2

from bs4 import BeautifulSoup

from constants import constants
from databases.QuotationDAO import QuotationDAO
from databases.SymbolDAO import SymbolDAO
from string import Template

base_uri = "https://es-us.finanzas.yahoo.com/quote/$symbol/$kind?p=$symbol"


class Kind(object):
    def __init__(self, name, tableClassName,period):
        self.name = name
        self.tableClassName = tableClassName
        self.period = period


kinds = [Kind("financials", "Lh(1.7) W(100%) M(0)",period="ingreso")
    , Kind("balance-sheet", "Lh(1.7) W(100%) M(0)",period="Final del período")
    , Kind("cash-flow", "Lh(1.7) W(100%) M(0)",period="Final del período")]

def populate_companies(market):
    symbolDAO = SymbolDAO(constants.databaseLocation)
    symbols = symbolDAO.select_all_symbols(market)
    import requests
    for symb in symbols:
        r = requests.post("http://localhost:8080/companies", data={'name': symb[0].__str__()}.__str__())
        print(r.status_code, r.reason)

def populate_accounts_by_symbol(market,tag="table",selector="tr td span"):
    symbolDAO = SymbolDAO(constants.databaseLocation)
    symbols = symbolDAO.select_all_symbols(market)
    for symb in symbols:
        # print "SYMBOL TO EXTRACT SYMB:=> " + symb[0]
        for kind in kinds:
            quote_page = Template(base_uri).substitute(dict(symbol=symb[0], kind=kind.name))
            html = urllib2.urlopen(quote_page).read()
            soup = BeautifulSoup(html)
            index = 0
            matrix = {}
            try:
                table = soup.findAll(tag)[0]
                spans = list(table.select(selector))
                while index < len(spans):
                    sp = spans[index]
                    sub_list = spans[index + 1: index + 5]
                    if is_key(sp.text) and validate_list(sub_list):
                        matrix[to_key(sp.text)] = [transform_ele(e.text) for e in list(sub_list)]
                        index += 5
                    else:
                        index += 1
                        # dar de alta la empresa
                        # dar de alta las cuentas para los periodos encontrados en yahoo

            except IndexError:
                print Template('''Oops! When try to scrapy $symb.
                                  This page not contains data with tag:$tag
                                  and selector $selector.Try again please''').substitute(dict(symb=symb[0],tag=tag,selector=selector))
            try:
                lastYear = int(matrix[to_key(kind.period)][0].strftime("%Y"))
                for key, values in matrix.iteritems():
                    if key != kind.period:
                        year = lastYear
                        for val in values:
                            import requests
                            r = requests.post("http://localhost:8080/accounts",
                                              data={'name': key.__str__(),
                                                    'value': val,
                                                    'year': year,
                                                    'companyName': symb[0].__str__()
                                                    }.__str__())
                            print(r.status_code, r.reason)
                            year -=1
            except KeyError:
                print Template('''Oops! When try add accounts in $symb.
                                                 This page not contains data with tag:$tag
                                                 and selector $selector.Try again please''').substitute(
                    dict(symb=symb[0], tag=tag, selector=selector))

            print matrix


def to_key(el):
    return re.sub('[^A-Za-z0-9]+', '', el.replace(" ", "0")).replace("0", "").lower()


def transform_ele(el):
    if is_date(el):
        return to_date(el)
    if is_number(el):
        return to_number(el)


def validate_list(list):
    res = True
    for e in list:
        if not (is_valid_element(e)):
            res = False
    return res


def is_valid_element(e):
    return is_number(e.text) or is_date(e.text)


def is_key(s):
    return not (is_number(s)) and not (is_date(s))


def to_number(s):
    try:
        return float(s.replace(",", ""))
    except ValueError:
        return False


def is_number(s):
    try:
        float(s.replace(",", ""))
        return True
    except ValueError:
        return False


def to_date(date_text):
    import datetime
    try:
        return datetime.datetime.strptime(date_text, '%d/%m/%Y')
    except ValueError:
        return False


def is_date(date_text):
    import datetime
    try:
        datetime.datetime.strptime(date_text, '%d/%m/%Y')
        return True
    except ValueError:
        return False


populate_companies('merval')
populate_accounts_by_symbol('merval')
