import pandas as pd

from autils.file_helper import make_dir
from autils.financials import get_data, plot_rolling_bands
from autils.plot_helper import save_figure
from autils.time_helper import get_now, get_delta, get_date_time
from data.process_data_stock.common_utils import get_symbols
from data.process_data_stock.compute_daily_return import RSI


def plot_bollinger_bands_withRSI(market, symbols, defaultSymbol, dt_start, dt_end, out_put):
    dates = pd.date_range(dt_start, dt_end)
    # Choose stock symbols to read
    # Get stock data
    df = get_data(market, symbols, dates, defaultSymbol)
    # Slice and plot
    plot_rolling_bands(df, base_dir=out_put, symbol=symbols[0], suffix='ROLL_BAND')
    save_figure(plot=RSI(df, symbols[0], period=14).plot(y=['RSI']),
                base_dir=out_put,
                symbol=symbols[0],
                suffix="RSI")
    pass


from data.constant_stock import ConstantsFile


def plot_all_bollinger_bands(market):
    now = get_now()
    dateAgo = get_delta(now, days=365 * 2)
    start = get_date_time(dateAgo)
    end = get_date_time(now)
    path = ConstantsFile.PATH_INDICATORS.format(market)
    make_dir(path)
    for symb in get_symbols(market):
        symbols = [symb]
        plot_bollinger_bands_withRSI(market=market,
                                     symbols=symbols,
                                     defaultSymbol=symb,
                                     dt_start=start.isoformat(),
                                     dt_end=end.isoformat(),
                                     out_put=path)


plot_all_bollinger_bands(market="merval")
