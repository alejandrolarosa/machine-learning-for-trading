import matplotlib.pyplot as plt
import pandas as pd

from autils.financials import get_data, compute_daily_returns, plot_data
from autils.plot_helper import save_figure
from autils.time_helper import get_now, get_delta, get_date_time
from constants.constants import databaseLocation
from data.process_data_stock.common_utils import get_symbols, fill_missing_values
from databases.ReturnDayDAO import ReturnDayDAO


def plot_histogram(market, symbol, dates, base_dir, default_sym='AMZN'):
    symbols = [symbol]
    df = get_data(market, symbols, dates)
    fill_missing_values(df)
    # plot_data(df)
    dr = compute_daily_returns(df)
    plot_data(dr, title="Daily returns")
    dr.hist(bins=20, label=default_sym)
    mean = dr[symbol].mean()
    std = dr[symbol].std()
    kurtosis = dr[symbol].kurtosis()
    print "symbol={}, mean={} , std={} , Kurtosis={}".format(symbol, mean, std, kurtosis)

    returnDayDAO = ReturnDayDAO(database=databaseLocation)
    import datetime
    returnDayDAO.insert_return_day(symbol, datetime.datetime.now().date(), mean, std, kurtosis)

    plt.axvline(mean, color='w', linestyle="dashed", linewidth=2)
    plt.axvline(std, color='r', linestyle="dashed", linewidth=2)
    plot = plt.axvline(-std, color='r', linestyle="dashed", linewidth=2)
    save_figure(plot=plot,
                base_dir=base_dir,
                symbol=symbol,
                suffix="HG")


def plot_all_histogram(market, default_sym):
    symbols = get_symbols(market)
    now = get_now()
    dateAgo = get_delta(now, days=365 * 1)
    start = get_date_time(dateAgo)
    end = get_date_time(now)
    dates = pd.date_range(start, end)
    path = ConstantsFile.PATH_INDICATORS.format(market)
    returnDayDAO = ReturnDayDAO(database=databaseLocation)
    returnDayDAO.create_return_day_table()


    for symb in symbols:
        plot_histogram(market=market,
                       symbol=symb,
                       dates=dates,
                       base_dir=path,
                       default_sym=default_sym)

    pass


from data.constant_stock import ConstantsFile

plot_all_histogram(market="sap", default_sym='APPL')
