import pandas as pd

from autils.file_helper import make_dir
from autils.financials import get_data
from autils.plot_helper import save_figure
from autils.time_helper import get_now, get_delta, get_date_time
from data.constant_stock import ConstantsFile
from data.process_data_stock.common_utils import get_symbols


def compute_macd(df, symb):
    df['30 mavg'] = pd.rolling_mean(df[symb], 30)
    df['26 ema'] = pd.ewma(df[symb], span=26)
    df['12 ema'] = pd.ewma(df[symb], span=12)
    df['MACD'] = (df['12 ema'] - df['26 ema'])
    df['Signal'] = pd.ewma(df['MACD'], span=9)
    df['Crossover'] = df['MACD'] - df['Signal']
    return symb, df['Crossover'][-1:].mean()


def plot_macd(market):
    path = ConstantsFile.PATH_INDICATORS.format(market)
    make_dir(path)
    now = get_now()
    dateAgo = get_delta(now, days=365 * 2)
    start = get_date_time(dateAgo)
    end = get_date_time(now)
    import csv
    with open("MACD_{}.csv".format(market), 'wb') as myfile:
        wr = csv.writer(myfile)
        wr.writerow(["Date", get_now()])
        wr.writerow(["Symbol", "MACD"])
        for symb in get_symbols(market):
            symbols = [symb]
            dates = pd.date_range(start, end)
            df = get_data(market, symbols, dates, defaultSymbol=symb)
            stock, macd = compute_macd(df, symb)
            wr.writerow([stock, macd])
            df.plot(y=[symb], title=symb)
            save_figure(plot=df.plot(y=['MACD'], title='MACD'),
                        base_dir=path,
                        symbol=symb,
                        suffix="MACD")
            print "save figure {} was Successful!".format(symb)

    pass

plot_macd(market="merval")
