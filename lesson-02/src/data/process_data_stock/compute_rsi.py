import pandas as pd

from autils.financials import get_data
from autils.time_helper import get_now, get_delta, get_date_time
from constants.constants import databaseLocation
from data.process_data_stock.common_utils import get_symbols
from databases.RsiDAO import RsiDAO


def relative_strength_index(df, period=14, symb="", type='simple'):
    # difference in price from preceding day
    delta = df[symb].diff()

    # Gain and loss in up and down periods
    up, down = delta.copy(), delta.copy()
    # set loss to zero in up periods
    up[up < 0] = 0
    # set gain to zero in down period
    down[down > 0] = 0

    if type == 'simple':
        rs = up.rolling(period).mean() / -down.rolling(period).mean()
        # rs = up.ewm(period).mean() / -down.ewm(period).mean()
    elif type == 'exponential':
        rs = up.ewm(span=period, adjust=False).mean() / -down.ewm(span=period, adjust=False).mean()
    return 100 - (100 / (1 + rs))


def compute_current_rsi(market, years, period=14):
    now = get_now()
    date_ago = get_delta(now, days=365 * years)
    start = get_date_time(date_ago)
    end = get_date_time(now)
    rsi_dao = RsiDAO(database=databaseLocation)
    dates = pd.date_range(start, end)
    for symbol in get_symbols(market):
        symbols = [symbol]
        df = get_data(market, symbols, dates, symbol)
        period = 14
        rsi_simple = relative_strength_index(df, period, symbol, type='simple')
        rsi_exponential = relative_strength_index(df, period, symbol, type='exponential')
        if len(rsi_simple) > 0:
            rsi_dao.insert_rsi(symbol=symbol,
                               dateOnly=dates[-1].date(),
                               simple=rsi_simple.iloc[-1],
                               exponential=rsi_exponential.iloc[-1])

    pass


compute_current_rsi(market="merval2", years=1, period=14)
