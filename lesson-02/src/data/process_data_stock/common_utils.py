from constants.constants import databaseLocation
from databases.SymbolDAO import SymbolDAO


def get_symbols(market="merval"):
    symbDAO = SymbolDAO(databaseLocation)
    result = list()
    for sym in symbDAO.select_all_symbols(market):
        result.append(sym[0])
    print result
    return result

def fill_missing_values(df_data):
    """Fill missing values in data frame, in place."""
    ##########################################################
    df_data.fillna(method="ffill")
    df_data.fillna(method="bfill")
    ##########################################################
