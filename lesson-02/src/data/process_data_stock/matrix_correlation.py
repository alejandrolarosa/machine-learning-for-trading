import pandas as pd

from autils.financials import get_data, compute_daily_returns
from autils.time_helper import get_now, get_delta, get_date_time
from data.process_data_stock.common_utils import fill_missing_values, get_symbols



def matriz_correlation(market):
    symbols = get_symbols(market)
    now = get_now()
    dateAgo = get_delta(now, days=365 * 1)
    start = get_date_time(dateAgo)
    end = get_date_time(now)
    dates = pd.date_range(start, end)
    df = get_data(market, symbols, dates)
    fill_missing_values(df)
    daily_returns = compute_daily_returns(df)
    df2 = pd.DataFrame(daily_returns)
    df2 = df2.pct_change()
    writer = pd.ExcelWriter("matrix_corr_{}.xlsx".format(market))
    df2.corr().to_excel(writer, 'correlation')
    writer.save()


matriz_correlation(market="merval2")
