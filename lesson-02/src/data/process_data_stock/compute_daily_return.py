import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from autils.file_helper import make_dir
from autils.financials import get_data, compute_daily_returns
from autils.plot_helper import save_figure
from autils.time_helper import get_now, get_delta, get_date_time
from data.process_data_stock.common_utils import get_symbols, fill_missing_values


def plot_daily_return(market, path, default_sym='YPFD.BA'):
    symbols = get_symbols(market)
    now = get_now()
    dateAgo = get_delta(now, days=365 * 2)
    start = get_date_time(dateAgo)
    end = get_date_time(now)
    dates = pd.date_range(start, end)
    df = get_data(market=market, symbols=symbols, dates=dates)
    fill_missing_values(df)
    daily_returns = compute_daily_returns(df)
    for symb in symbols:
        try:
            plot = daily_returns.plot(kind='scatter', x=default_sym, y=symb)
            beta_XOM, alphe_XOM = np.polyfit(x=daily_returns[default_sym], y=daily_returns[symb], deg=1)
            plt.plot(daily_returns[default_sym], beta_XOM * daily_returns[default_sym] + alphe_XOM, '-', color='r')
            save_figure(plot=plot,
                        base_dir=path,
                        symbol=symb,
                        suffix="DR")
        except ValueError:
            print "Oops!  That was no valid number.  Try again..."
    pass


from data.constant_stock import ConstantsFile

def plot_daily_return_one_for_all(market):
    symbols = get_symbols(market)
    for symb in symbols:
        path = ConstantsFile.PATH_INDICATORS.format(market) + "/" + symb
        make_dir(path)
        plot_daily_return(market=market, path=path, default_sym=symb)
    pass


plot_daily_return_one_for_all(market="sap")
