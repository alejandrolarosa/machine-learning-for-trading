import json
from constants import  constants
from databases.SymbolDAO import SymbolDAO
from extract_yahoo_finance import get_historical_data
import threading
import os
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

class Market():
    def __init__(self,  **kwargs):
        self.name = kwargs.get('name')
        self.exluded_symbols = kwargs.get('excluded_symbols')
        self.years = kwargs.get('years')
        self.days = kwargs.get('days')

    # second step populate all symbol by market.
    # see json files merval.json and cedears.json
    def populate_symbols(self):
        self.save_symbol()

    def save_quotation_stock(self):
        symbolDAO = SymbolDAO(constants.databaseLocation)
        for sym in symbolDAO.select_all_symbols(self.name):
            if not sym[0] in self.exluded_symbols:
                t1 = SaveQuotationThread(get_historical_data, sym[0], self.name, (self.years * 365) + self.days, symbolDAO.get_connection())
                t1.start()
                t1.join()

    def get_yahoo_data_by_market(self):
        # save all quotation for last 2 years ago
        excluded = list()
        # excluded = QuotationDAO(constants.databaseLocation,connection=SymbolDAO(constants.databaseLocation).getConnection()).get_symbols_with_quotations()
        self.save_quotation_stock()

    def save_symbol(self, suffix_symbol=".BA"):
        symbolDAO = SymbolDAO(constants.databaseLocation)
        path = ROOT_DIR + "/../json/" + self.name + '.json'
        print path
        with open(path) as data_file:
            data = json.load(data_file)
            stocks = data[self.name]
            listOfSymbols = list()
            for s in stocks['stocks']:
                # symbol = s['desc'].split("-")[0].strip()
                symbol = s['name']
                if 'symbol' not in s:
                    # sym = symbol + suffix_symbol
                    sym = symbol
                    s['symbol'] = sym
                    listOfSymbols.append(symbolDAO.insert_symbol(sym, self.name))


class SaveQuotationThread(threading.Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        threading.Thread.__init__(self)

    def run(self):
        self._target(*self._args)

