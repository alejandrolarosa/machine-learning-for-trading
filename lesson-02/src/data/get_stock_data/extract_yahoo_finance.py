import pandas_datareader as pdr
from pandas_datareader._utils import RemoteDataError

from autils.time_helper import get_now, get_date_time, get_delta
from constants import constants
from databases.QuotationDAO import QuotationDAO


def get_historical_data(symbol, market, daysAgo, connection):
    quote_dao = QuotationDAO(constants.databaseLocation, connection)
    now = get_now()
    datesAgo = get_delta(now, daysAgo)
    import time
    time.sleep(5)
    try:
        dr = pdr.get_data_yahoo(symbols=symbol,
                            # data_source='google',
                            start=get_date_time(datesAgo),
                            end=get_date_time(now))
        import datetime
        # dr = dr.set_index('Time')
        # dr.index = dr.index.to_datetime()
        for index, row in dr.iterrows():
            print(index.__str__())
            quote_dao.insert_quotation(symbol=symbol,
                                       market= market,
                                       dateOnly=datetime.datetime.strptime(index.__str__(), "%Y-%m-%d %H:%M:%S"),
                                       open_price=row['Open'],
                                       high=row['High'],
                                       low=row['Low'],
                                       close=row['Close'],
                                       adj_close=row['Close'],
                                       volume=row['Volume'])
            print "success insert :", (symbol, row['Close'], row['Open'])
        # dr.to_csv(file)
        print "success " + symbol
        return file
    except RemoteDataError,e:
        print "Oops!  We have some Remote data error. " + symbol
        print e
        return "FAIL"
