# HOW TO USE THIS PROYECT

## Execute Main class

### Create database (See Main.py)
This class create schema database using SQLiteDb
and create 3 tables. quotation,rsi and symbol
Quotation contains resume of main values provided by Yahoo finance
rsi contains value of this indicators by period
symbol contains all symbol by market extracted from json with same name

### Populate all symbol
* We use json files.
* This jsons were created using the homebanking of SANTANDERIO. in the following path santanderio.com/inversiones/acciones (get_stocks_from_yahoo)
* You can inspect web and copy the tag select with the options. Only we need to add .BA in each symbol parsed the full name.

### Populate data obtained
*  These data was persited in our db (SQLite) in above step.
* Compute histogram to compare all symbols // nos sirve para comparar entre symbolos e identificar cuales tienen mayor o menor volatilidad.
* compute daily returns   // nos sirve para calcular la correlacion
* Compute MACD indicator // muestra con anticipacion los momentos donde cambia el sentido de la tendencia.
* Compute RSI indicator // debajo de 30 es buen valor para comprar luego de diverger con la tendencia del precio por encima de 70 para vender con la misma apreciacion.
* Compute Matrix Correlation // sirve para comparar symbolos los que tengan menor correlacion son las mas divergentes y aptas para formar una cartera

## TODO
* add  Williams indicator
* add Montecarlo simulation o similiar simulation





