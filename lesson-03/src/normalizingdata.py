import os

import pandas as pd
import matplotlib.pyplot as plt


def symbol_to_path(symbol, base_dir="data"):
    return os.path.join(base_dir, "{}.csv".format(str(symbol)))


def get_data(symbols, dates):
    df = pd.DataFrame(index=dates)
    if 'SPY' not in symbols:
        symbols.insert(0, 'SPY')

    for symbol in symbols:
        df_temp = pd.read_csv(symbol_to_path(symbol),
                              index_col="Date",
                              parse_dates=True,
                              usecols=['Date', 'Adj Close'],
                              na_values=['nan'])
        df_temp = df_temp.rename(columns={'Adj Close': symbol})
        df = df.join(df_temp)
        if symbol == 'SPY':
            df = df.dropna(subset=["SPY"])

    return df


def plot_data(df, title="Stock Prices"):
    ax = df.plot(title=title, fontsize=2)
    ax.set_xlabel("Date")
    ax.set_ylabel("Price")
    plt.show()


def plot_selected(df, columns, start_date, end_date):
    plot_data(df.ix[start_date:end_date, columns], title="Selected Data")


def normalize_data(df):
    return df / df.ix[0, :]


def test_run():
    print "pandas version is: ", pd.__version__
    # Define date range
    start_date = '2016-10-12'
    end_date = '2017-10-12'
    dates = pd.date_range(start_date, end_date)
    symbols = ['GOOG', 'IBM', 'GLD']

    df = get_data(symbols, dates)
    # print df.ix['2017-09-26': '2017-10-09',["SPY","IBM"]]
    print df.keys()
    plot_selected(normalize_data(df), symbols, start_date, end_date)



if __name__ == "__main__":
    test_run()
